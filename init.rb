Redmine::Plugin.register :redmine_static_link do
  name 'redmine_top_menu_link'
  author 'Sergey Pronyuk (amcbridge)'
  description 'Links on the top menu'

  version '0.1.0'
  
  menu :top_menu, :tts, "https://tts.amcbridge.com", :html => { :target => '_blank' }, :after => :projects
  menu :top_menu, :wpm, "https://wpm.amcbridge.com", :html => { :target => '_blank' }
  menu :top_menu, :hrms, "https://hrms.amcbridge.com", :html => { :target => '_blank' }
  menu :top_menu, :helpdesk, "http://ithelp.amcbridge.com", :html => { :target => '_blank' }
end

