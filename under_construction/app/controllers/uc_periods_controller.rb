class UcPeriodsController < ApplicationController

  layout 'admin'

  before_filter :require_admin, except: [:under_construction]

  def index
    @uc_period = find_default_period
    if @uc_period.nil?
      @uc_period = UcPeriod.new
    end
    @routes ||= UcRoute.routes
    @controllers ||= UcRoute.controllers
    render 'show'
  end

  def add_restriction
    @routes ||= UcRoute.routes
    @controllers ||= UcRoute.controllers
    @timestamp = Time.now.to_f.to_s.sub(/\./, '_')
  end

  def delete_restriction
  end

  def create
    @uc_period = UcPeriod.new(params[:uc_period])
    respond_to do |format|
      if @uc_period.save
        @uc_period.notify_users if params[:notify_users]

        flash[:notice] = l(:notice_tech_period_saved)
        format.html { redirect_to action: 'index' }
      else
        format.html { render 'show' }
      end
    end
  end

  def update
    @uc_period = UcPeriod.find(params[:id])

    begin_date = (params[:uc_period].delete('begin_date(1i)') + '-' +
                  params[:uc_period].delete('begin_date(2i)') + '-' +
                  params[:uc_period].delete('begin_date(3i)') + ' ' +
                  params[:uc_period].delete('begin_date(4i)') + ':' +
                  params[:uc_period].delete('begin_date(5i)')).to_time

    end_date = (params[:uc_period].delete('end_date(1i)') + '-' +
                params[:uc_period].delete('end_date(2i)') + '-' +
                params[:uc_period].delete('end_date(3i)') + ' ' +
                params[:uc_period].delete('end_date(4i)') + ':' +
                params[:uc_period].delete('end_date(5i)')).to_time

    if begin_date && end_date
      zone = User.current.time_zone
      begin_date = zone ? begin_date.in_time_zone(zone) : (begin_date.utc? ? begin_date.localtime : begin_date)
      end_date = zone ? end_date.in_time_zone(zone) : (end_date.utc? ? end_date.localtime : end_date)
    end

    respond_to do |format|
      if @uc_period.update_attributes(params[:uc_period].merge({begin_date: begin_date, end_date: end_date}))
        @uc_period.notify_users if @uc_period.notify
        flash[:notice] = l(:notice_tech_period_saved)
        format.html { redirect_to action: 'index' }
      else
        format.html { render 'show' }
      end
    end
  end

  def under_construction
    @uc_period = find_default_period
    render 'under_construction', layouts: false
  end

  def preview
    @notes = params[:preview_text]
    render layouts: false
  end

  private

  def find_default_period
    @uc_period = UcPeriod.includes(:uc_restrictions).order('begin_date desc').first
  end

  def require_admin
    render_403 unless User.current.admin?
  end

end
