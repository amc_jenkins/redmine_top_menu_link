module UnderConstructionHelper
  def uc_format_time_to_date(date, options={})
    if Redmine::VERSION.to_s >= '3.3.0'
      format_time(date, true, options[:user]).split(' ')[0]
    else
      format_time(date, true).split(' ')[0]
    end
  end

  def uc_format_time_to_datetime(date, options={})
    if Redmine::VERSION.to_s >= '3.3.0'
      format_time(date, true, options[:user]).split(' ')[1]
    else
      format_time(date, true).split(' ')[1]
    end
  end
end