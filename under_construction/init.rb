Redmine::Plugin.register :under_construction do
  name 'Under Construction'
  author 'Danil Kukhlevskiy'
  description 'This is a plugin for temporary disable RM pages'
  version '2.0.3'
  url 'http://rmplus.pro/redmine/plugins/under_construction'
  author_url 'http://rmplus.pro/'

  menu :admin_menu, :admin_menu_under_construction, { controller: :uc_periods, action: :index }, caption: :admin_menu_under_construction
  menu :admin_menu, :admin_menu_browsers_restricitons, { controller: :settings, action: :plugin, id: :under_construction }, caption: :admin_menu_browsers_restricitons

  settings  partial: 'settings/uc_settings',
            default: { 'responsible_ids' => []}
end

require 'under_construction/view_hooks'

Rails.application.config.to_prepare do
  ApplicationController.send(:include, UnderConstruction::ApplicationControllerPatch) unless ApplicationController.included_modules.include?(UnderConstruction::ApplicationControllerPatch)
  ActionView::Base.send(:include, UnderConstructionHelper)
end

Rails.application.config.after_initialize do
  plugins = { a_common_libs: '2.1.9' }
  plugin = Redmine::Plugin.find(:under_construction)
  plugins.each do |k,v|
    begin
      plugin.requires_redmine_plugin(k, v)
    rescue Redmine::PluginNotFound => ex
      raise(Redmine::PluginNotFound, "Plugin requires #{k} not found")
    end
  end
end